git_dir := "$HOME/git"
project_name := "automate-development"
group_name := "the-integrative-driver-config"
group_dir := git_dir / group_name
project_dir := group_dir / project_name
config_file := project_dir / "config.toml"
just_file_git := project_dir / "justfile"
just_file_loc := group_dir / "justfile"

# Interactive menu to choose recipe
default:
	@just --choose

# Open group web page
web:
	#!/bin/bash
	xdg-open "https://gitlab.com/{{group_name}}"
	# the following is unique to my setup, where browser is in workspace 1
	xdotool key alt+1


# Update files for automate development tool
push:
	#!/bin/bash
	grm repos find local {{group_dir}} > {{config_file}}
	if [ ! -f "{{just_file_loc}}" ];
	then
		cp "{{just_file_git}}" "{{just_file_loc}}"
		echo "Created justfile"
	else
		cp "{{just_file_loc}}" "{{just_file_git}}"
		echo "Updated justfile"
		cd {{project_dir}}
		git add justfile config.toml
	fi

# Edit this configuration
edit: 
	#!/bin/bash
	nvim {{just_file_loc}}

# Copy project to destination:
template source target:
	#!/bin/bash
	#cp -r --backup {{source}} {{target}}

# Testing out this configuration without problems
test:
	#!/bin/bash
	IMAGE="automate-development/myfedora:38"
	docker build -t $IMAGE .
	docker run -it $IMAGE bash 

# Create new repository for this group
create repo: && group 
	#!/bin/bash
	glab repo create --group {{group_name}} {{repo}}

# Delete repository if exsits and remove project directory 
delete repo: 
	#!/bin/bash
	glab repo delete "{{group_name}}/{{repo}}" && \ 
	rm -Ir "{{group_dir}}/{{repo}}"
	grm repos find local {{group_dir}} > {{config_file}}

# Clone all projects inside your group
group:
	cd {{git_dir}}
	glab repo clone -p -g {{group_name}}

# Initialize: ./scripts/init
init:
	#!/bin/bash	
	ansible-playbook -i localhost, --connection=local -bK {{project_dir}}/ansible/install.yml
