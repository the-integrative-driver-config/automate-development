# Automate development

This project is to automate development inside this project (first) and make templating easy to do within gitlab. Like what you can see here [group level project templates](https://docs.gitlab.com/ee/user/group/custom_project_templates.html). The reason being that I have some projects that I build in isolation that can be used as templates. Also I want to have a easy time setting up projects if I where to change my driver on which I am developing.

Developed with fedora 38
## Getting started

Clone repository:
``` shell
git clone git@gitlab.com:the-integrative-driver-config/automate-development.git
cd automate-development
./script/init
```

## Usage
These are the your recipes:
``` bash
just -l
Available recipes:
    create repo            # Create new repository for this group
    default                # Interactive menu to choose recipe
    delete repo            # Delete repository if exsits and remove project directory
    edit                   # Edit this configuration
    group                  # Clone all projects inside your group
    init                   # Initialize: ./scripts/init
    push                   # Update files for automate development tool
    template source target # Copy project to destination:
    test                   # Testing out this configuration without problems
    web                    # Open group web page
```
