FROM fedora:38

ARG USERNAME=theintegrative

RUN dnf update -y
RUN useradd $USERNAME
RUN usermod -aG wheel $USERNAME
COPY ./ /home/$USERNAME/git/automate-development/

RUN echo "$USERNAME ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
RUN chown $USERNAME:$USERNAME -R /home/$USERNAME/

USER $USERNAME
WORKDIR "/home/$USERNAME/git/automate-development/"

RUN ./scripts/init
